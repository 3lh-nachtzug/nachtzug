FROM python:3.7
ENV PYTHONUNBUFFERED 1
ENV PIPENV_VENV_IN_PROJECT=1
ENV FLASK_APP=app/__init__.py

RUN mkdir /app
WORKDIR /app

COPY Pipfile.lock Nachtzug /app/
RUN pip install pipenv \
  && pipenv install --ignore-pipfile

CMD ["pipenv", "run", "flask", "run"]

from flask import request
from flask_appbuilder.api import BaseApi, expose, rison, safe
from flask_appbuilder.security.decorators import protect

from . import appbuilder

import urllib.request
import json
import requests

import datetime
import time

greeting_schema = {"type": "object", "properties": {"name": {"type": "string"}}}

# Get data from src, keep global for all functions
trainlistURL = "https://www.oebb.at/reiseportal-apps/train/"
response = requests.get(trainlistURL + 'list/')
stations = json.loads(response.text)['stations']
timetable = json.loads(response.text)['timetable']

def get_station_data(station_no):
    fields = ['eva', 'name', 'lat', 'lng']
    station_data = [s for s in stations if s['eva'] == str(station_no)][0]
    station = {}

    for f in fields:
        if f in station_data:

            # Special treatment for GPS coordinates
            if f == 'lat' or f == 'lng':
                try:
                    ret_val = float(station_data[f]) / pow(10, 6)
                except:
                    ret_val = station_data[f]
            else:
                ret_val = station_data[f]

            # Add data to dict
            station.update([(f, ret_val)])

    return station
  

class trainiteAPI(BaseApi):
  resource_name = "trainite"
  apispec_parameter_schemas = {"greeting_schema": greeting_schema}

  # Basic station list showing station no, name and GPS coordinates
  @expose("/stationlist", methods=["POST", "GET"])
  def stations(self):
    station_list = [get_station_data(s['eva']) for s in stations]

    return self.response(200, message=station_list)


  @expose("/destinations", methods=["POST", "GET"])
  def destinations(self):
    args = request.args

    bhfDepNo = int(args['bhfDep'])

    train_nos = []
    possible_destinations_tmp = []
    for tt in timetable:
        stops = tt['stops']

        i = 0
        station_found = False
        # Search for bhfDepNo in all stops occuring in the timetable
        for s in stops:

          # Stop is in timetable, so this train is relevant
          if s['eva'] == bhfDepNo:
              train_nos.append(tt['trainNumber'])
              station_found = True
          
          # Next stops after this are possible destinations for this departure station
          if station_found == True and s['eva'] != bhfDepNo:
              possible_destinations_tmp.append(s['eva'])

          i = i + 1
    
    # Distinct list
    possible_destinations_tmp = list(set(possible_destinations_tmp))

    # Get detailed data for each possible destination station
    possible_destinations = []
    for pb in possible_destinations_tmp:
        possible_destinations.append(get_station_data(pb))
        # possible_destinations.append(pb)

    return self.response(200, message=possible_destinations)


  @expose("/timetable", methods=["POST", "GET"])
  def timetable(self):
    args = request.args

    bhfDepNo = int(args["bhfDep"])
    bhfArrNo = int(args["bhfArr"])

    # Find possible trains which travel from bhfDepNo to bhfArrNo
    possible_trains = []
    for tt in timetable:
        stops = tt['stops']

        i = 0
        bhf_dep_found = False
        bhf_arr_found = False
        for s in stops:
          # Departure stop is in timetable
          if s['eva'] == bhfDepNo:
              bhf_dep_found = True
        
          # Arrival stop is in timetable and after departure stop, so this train is relevant
          if bhf_dep_found ==  True and s['eva'] == bhfArrNo:
              bhf_arr_found = True
              possible_trains.append(tt['trainNumber'])
              break

    # Get possible connections from bhfDepNo to bhfArrNo, i.e. list detailed data
    # for train numbers found
    possible_connections = []
    for pt in possible_trains:
        connection = {}
        for tt in timetable:
            if tt['trainNumber'] == pt:
                connection.update([('trainNumber', tt['trainNumber'])])

                # Get departure info, add to dict
                time_dep = [s['dep'] for s in tt['stops'] if s['eva'] == bhfDepNo][0]
                lat_dep = [s['lat'] / pow(10, 6) for s in tt['stops'] if s['eva'] == bhfDepNo][0]
                lng_dep = [s['lng'] / pow(10, 6) for s in tt['stops'] if s['eva'] == bhfDepNo][0]
                name_dep = [s['name'] for s in tt['stops'] if s['eva'] == bhfDepNo][0]
                connection.update([('time_dep', time_dep)])
                connection.update([('lat_dep', lat_dep)])
                connection.update([('lng_dep', lng_dep)])
                connection.update([('name_dep', name_dep)])

                # Get arrival info, add to dict
                time_arr = [s['arr'] for s in tt['stops'] if s['eva'] == bhfArrNo][0]
                lat_arr = [s['lat'] / pow(10, 6) for s in tt['stops'] if s['eva'] == bhfArrNo][0]
                lng_arr = [s['lng'] / pow(10, 6) for s in tt['stops'] if s['eva'] == bhfArrNo][0]
                name_arr = [s['name'] for s in tt['stops'] if s['eva'] == bhfArrNo][0]
                connection.update([('time_arr', time_arr)])
                connection.update([('lat_arr', lat_arr)])
                connection.update([('lng_arr', lng_arr)])
                connection.update([('name_arr', name_arr)])

                dep = datetime.datetime.combine(datetime.date(1970, 1, 1), datetime.datetime.strptime(time_dep, "%H:%M").time())
                arr = datetime.datetime.combine(datetime.date(1970, 1, 2), datetime.datetime.strptime(time_arr, "%H:%M").time())
                duration = int((arr - dep).total_seconds())
                hours, remainder = divmod(duration, 60*60)
                minutes, seconds = divmod(remainder, 60)

                # Add further info on train
                connection.update([('operator', tt['operator'])])
                connection.update([('season', tt['season'])])
                connection.update([('priceFrom', tt['priceFrom'])])
                connection.update([('duration', '{}:{}'.format(hours,minutes))])

        possible_connections.append(connection)

    return self.response(200, message=possible_connections)

appbuilder.add_api(trainiteAPI)
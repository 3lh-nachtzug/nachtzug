import urllib.request
import json
import requests

trainlistURL = "https://www.oebb.at/reiseportal-apps/train/"
# #response = urllib.request.urlopen(trainlistURL + 'list/')
# response = requests.get(trainlistURL + 'list/')
# stations = json.loads(response.text)['stations']

# fields = ['eva', 'name', 'lat', 'lng']
# station_list = []
# for s in stations:
#     station = {}
#     for f in fields:
#         if f in s:
#             station.update([(f, s[f])])
#     station_list.append(station)

# print(station_list)

def get_station_data(station_no):
    response = requests.get(trainlistURL + 'list/')
    stations = json.loads(response.text)['stations']

    fields = ['eva', 'name', 'lat', 'lng']
    station = {}

    for s in stations:
       if s['eva'] == str(station_no):
            for f in fields:
                if f in s:

                    # Special treatment for GPS coordinates
                    if f == 'lat' or f == 'lng':
                        try:
                            ret_val = float(s[f]) / pow(10, 6)
                        except:
                            ret_val = s[f]
                    else:
                        ret_val = s[f]

                    station.update([(f, ret_val)])

    return station

response = requests.get(trainlistURL + 'list/')
ttables = json.loads(response.text)['timetable']

dbgno = 8100072

train_nos = []

possible_destinations_tmp = []
for tt in ttables:
    stops = tt['stops']

    i = 0
    station_found = False
    for s in stops:
        if s['eva'] == dbgno:
            train_nos.append(tt['trainNumber'])
            station_found = True
        
        if station_found == True and s['eva'] != dbgno:
            possible_destinations_tmp.append(s['eva'])

        i = i + 1

possible_destinations_tmp = list(set(possible_destinations_tmp))

possible_destinations = []
for pb in possible_destinations_tmp:
    possible_destinations.append(get_station_data(pb))

print(possible_destinations)
# print(stops)